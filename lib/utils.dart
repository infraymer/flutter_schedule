import 'dart:ui';

import 'package:flutter_schedule/entity/lesson.dart';

Color getTypeColor(int typeId) {
  switch (typeId) {
    case 1:
      return Color(0xFF4E95F4);
      break;
    case 2:
      return Color(0xFF2DA9BF);
      break;
    case 3:
      return Color(0xFF77CB1C);
      break;
    case 4:
      return Color(0xFF815BDE);
      break;
    case 5:
      return Color(0xFFEE4364);
      break;
    case 6:
      return Color(0xFFFB9500);
      break;
    default:
      return Color(0xFF915437);
  }
}

Map<String, List<Lesson>> fixSchedule(Map<String, List<Lesson>> map) {
  for (var i = 0; i < map.length; i++) {
    var lessons = map.values.elementAt(i);

    lessons[0].addLectorLocation(lessons[0].lectorName, lessons[0].location);
    for (int j = 1; j < lessons.length; j++) {
      String currentTimeBegin = lessons[j].timeBegin;
      String currentSubject = lessons[j].subject;
      String currentSubgroup = lessons[j].subgroup;
      String previousTimeBegin = lessons[j - 1].timeBegin;
      String previousSubject = lessons[j - 1].subject;
      String previousSubgroup = lessons[j - 1].subgroup;

      if (currentSubject == previousSubject &&
          currentTimeBegin == previousTimeBegin &&
          currentSubgroup == previousSubgroup) {
        lessons[j - 1]
            .addLectorLocation(lessons[j].lectorName, lessons[j].location);
        lessons.removeAt(j);
        j--;
      }
    }
  }
  return map;
}

// fun getTypeColor(typeId) = when (type.id) {
//     1 -> Color.parseColor("#4e95f4")
//     2 -> Color.parseColor("#2da9bf")
//     3 -> Color.parseColor("#77cb1c")
//     4 -> Color.parseColor("#815bde")
//     5 -> Color.parseColor("#ee4364")
//     6 -> Color.parseColor("#fb9500")
//     else -> Color.parseColor("#915437")
// }
