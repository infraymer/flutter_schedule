class Lesson {
  String date;
  String timeBegin;
  String timeEnd;
  String subject;
  int typeId;
  String typeName;
  int lectorId;
  String lectorName;
  int groupId;
  String groupName;
  String subgroup;
  String location;
  List<LectorLocation> lectorLocation = List();
  List<dynamic> groups = List();

  Lesson(
      {this.date,
      this.timeBegin,
      this.timeEnd,
      this.subject,
      this.typeId,
      this.typeName,
      this.lectorId,
      this.lectorName,
      this.groupId,
      this.groupName,
      this.subgroup,
      this.location,
      this.groups});

  void addLectorLocation(String lector, String loc) {
    lectorLocation.add(LectorLocation(lector, location));
  }

  factory Lesson.fromJson(Map<String, dynamic> json) {
    var type = json["lesson_type"] as Map<String, dynamic>;
    var lector = json["lector"] as Map<String, dynamic>;
    var group = json["group"] as Map<String, dynamic>;
    var subgroup = group == null ? null : group["subgroup"] as Map<String, dynamic>;
    var subgroupName =
        subgroup == null ? null : subgroup["subgroup_name"] as String;
    var groups = (json["groups"] as List);
    if (groups != null) groups.map((item) => item as Map<String, dynamic>);
    return Lesson(
      date: json["date"] as String,
      timeBegin: json["time_begin"] as String,
      timeEnd: json["time_end"] as String,
      subject: json["subject_name"] as String,
      typeId: type["id"] as int,
      typeName: type["name"] as String,
      lectorId: lector == null ? null : lector["lector_id"] as int,
      lectorName: lector == null ? null : lector["lector_name"] as String,
      groupId: group == null ? null : group["group_id"] as int,
      groupName: group == null ? null : group["group_name"] as String,
      subgroup: subgroupName,
      location: json["location"] as String,
      groups: groups
    );
  }
}

class LectorLocation {
  String lector;
  String location;

  LectorLocation(this.lector, this.location);
}
