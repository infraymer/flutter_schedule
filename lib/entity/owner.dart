import 'package:json_annotation/json_annotation.dart';

part 'owner.g.dart';

@JsonSerializable()
class Owner {
  int id;
  String name;
  bool isGroup;

  Owner({this.id, this.name, this.isGroup});

  factory Owner.fromJson(Map<String, dynamic> json, bool group) {
    return Owner(
      id: json["id"] as int,
      name: json["name"] as String,
      isGroup: group
    );
  }

  factory Owner.fromJson2(Map<String, dynamic> json) => _$OwnerFromJson(json);

  Map<String, dynamic> toJson() => _$OwnerToJson(this);
}
