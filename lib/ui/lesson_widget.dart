import 'package:flutter/material.dart';
import 'package:flutter_schedule/entity/lesson.dart';
import 'package:flutter_schedule/utils.dart' as utils;

class LessonWidget extends StatefulWidget {
  Lesson lesson;
  bool isGroup;

  LessonWidget({Key key, this.lesson, this.isGroup}) : super(key: key);

  @override
  _LessonWidgetState createState() => _LessonWidgetState();
}

class _LessonWidgetState extends State<LessonWidget> {
  final _textStyle = TextStyle(fontSize: 16.0);

  var _isExpanded = false;

  String _timeBegin() => widget.lesson.timeBegin.substring(0, 5);

  String _timeEnd() => widget.lesson.timeEnd.substring(0, 5);

  Column _lectorLocationWidget(String lec, String loc) {
    return Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
            child: Row(
              children: <Widget>[
                Icon(Icons.person, color: Colors.black54),
                Text(lec, style: TextStyle(color: Colors.black54))
              ],
            )),
        Container(
          margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
          child: Row(
            children: <Widget>[
              Icon(Icons.place, color: Colors.black54),
              Text(loc, style: TextStyle(color: Colors.black54)),
            ],
          ),
        )
      ],
    );
  }

  List<Widget> _lectorLocationList() {
    var list = List<Widget>();
    for (var i = 1; i < widget.lesson.lectorLocation.length; i++) {
      list.add(_lectorLocationWidget(widget.lesson.lectorLocation[i].lector,
          widget.lesson.lectorLocation[i].location));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    var typeWidgets = List<Widget>();
    typeWidgets.add(
      Container(
        decoration: BoxDecoration(
            color: utils.getTypeColor(widget.lesson.typeId),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(2.0),
                bottomLeft: Radius.circular(2.0),
                topRight:
                    Radius.circular(widget.lesson.subgroup == null ? 2.0 : 0.0),
                bottomRight: Radius.circular(
                    widget.lesson.subgroup == null ? 2.0 : 0.0))),
        child: Text(
          widget.lesson.typeName,
          style: TextStyle(color: Colors.white),
        ),
        padding: EdgeInsets.fromLTRB(4.0, 2.0, 4.0, 2.0),
      ),
    );
    if (widget.lesson.subgroup != null)
      typeWidgets.add(
        Container(
          decoration: BoxDecoration(
              color: Colors.deepPurple,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(2.0),
                  bottomRight: Radius.circular(2.0))),
          child: Text(
            widget.lesson.subgroup,
            style: TextStyle(color: Colors.white),
          ),
          padding: EdgeInsets.fromLTRB(4.0, 2.0, 4.0, 2.0),
        ),
      );

    return Container(
        margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(color: Colors.deepPurple),
                      child: Text('${_timeBegin()}',
                          style:
                              TextStyle(fontSize: 16.0, color: Colors.white)),
                      margin: EdgeInsets.fromLTRB(8.0, 8.0, 0.0, 8.0),
                      padding: EdgeInsets.fromLTRB(4.0, 0.0, 4.0, 0.0),
                    ),
                    Container(
                      child: Text(' - ${_timeEnd()}',
                          style: TextStyle(
                              fontSize: 16.0, color: Colors.deepPurple)),
                      margin: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                    ),
                  ],
                ),
                Card(
                    shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    child: Row(children: typeWidgets))
              ],
            ),
            Container(
              padding: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 2.0),
              alignment: Alignment.centerLeft,
              child: Text(widget.lesson.subject, style: _textStyle),
            ),
            Row(children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.person, color: Colors.black54),
                          Text(widget.isGroup ? widget.lesson.lectorName : widget.lesson.groups[0]["group_name"] as String,
                              style: TextStyle(color: Colors.black54))
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.place, color: Colors.black54),
                        Text(widget.lesson.location,
                            style: TextStyle(color: Colors.black54)),
                      ],
                    ),
                  ),
                ],
              ),
              widget.lesson.lectorLocation.length > 1
                  ? IconButton(
                      padding: EdgeInsets.all(0.1),
                      icon: Icon(
                          !_isExpanded ? Icons.expand_more : Icons.expand_less),
                      onPressed: () {
                        setState(() {
                          _isExpanded = !_isExpanded;
                        });
                      },
                    )
                  : Text(""),
            ]),
            Column(
              children: _isExpanded ? _lectorLocationList() : List(),
            )
          ],
        ));
  }
}
