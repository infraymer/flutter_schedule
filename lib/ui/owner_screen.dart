import 'package:flutter/material.dart';
import 'package:flutter_schedule/entity/owner.dart';
import 'package:flutter_schedule/model/schedule_repository.dart';
import 'package:loader_search_bar/loader_search_bar.dart';

class OwnerScreen extends StatefulWidget {

  @override
  _OwnerScreenState createState() => _OwnerScreenState();
}

class _OwnerScreenState extends State<OwnerScreen> {

  List<Owner> _search = List(0);
  List<Owner> _owners = List(0);

  @override
  void initState() {
    super.initState();

    getOwners()
        .then((owners) {
          setState(() {
            _search = owners;
            _owners = owners;
          });
        })
        .catchError((err) =>
        print(err));
  }

  void _onSearchClicked(BuildContext context, String value) {
    setState(() {
      _search = (value == "") ? _owners : _owners.where((owner) => owner.name.toLowerCase().contains(value.toLowerCase())).toList();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SearchBar(
        onActivatedChanged: (isActivated) => _onSearchClicked(null, ""),
        defaultBar: AppBar(title: Text("Группы/преподаватели"),),
        onQueryChanged: (query) => _onSearchClicked(context, query),
        onQuerySubmitted: (query) => _onSearchClicked(context, query),
      ),
      body: _owners.isEmpty
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: _search.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text('${_search[index].name}'),
                  onTap: (() => Navigator.pop(context, _search[index])),
                );
              },
            ),
    );
  }
}
