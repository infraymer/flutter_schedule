import 'package:flutter/material.dart';
import 'package:flutter_schedule/entity/lesson.dart';
import 'package:flutter_schedule/utils.dart' as utils;

class LessonWidgetStls extends StatelessWidget {
  Lesson lesson;
  bool isGroup;

  LessonWidgetStls({Key key, this.lesson, this.isGroup}) : super(key: key);
  final _textStyle = TextStyle(fontSize: 16.0);

  var _isExpanded = false;

  String _timeBegin() => lesson.timeBegin.substring(0, 5);

  String _timeEnd() => lesson.timeEnd.substring(0, 5);

  TableRow _lectorLocationWidget(String lec, String loc) {
    return TableRow(children: <Widget>[
      TableCell(
        child: Row(children: <Widget>[
          Icon(Icons.person, color: Colors.black54),
          Text(lec, style: TextStyle(color: Colors.black54))
        ]),
      ),
      TableCell(
          child: Row(
        children: <Widget>[
          Icon(Icons.place, color: Colors.black54),
          Text(loc, style: TextStyle(color: Colors.black54)),
        ],
      )),
    ]);
  }

  List<TableRow> _lectorLocationList() {
    var list = List<TableRow>();
    for (var i = 0; i < lesson.lectorLocation.length; i++) {
      list.add(_lectorLocationWidget(
          lesson.lectorLocation[i].lector, lesson.lectorLocation[i].location));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    var typeWidgets = List<Widget>();
    typeWidgets.add(
      Container(
        decoration: BoxDecoration(
            color: utils.getTypeColor(lesson.typeId),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(2.0),
                bottomLeft: Radius.circular(2.0),
                topRight: Radius.circular(lesson.subgroup == null ? 2.0 : 0.0),
                bottomRight:
                    Radius.circular(lesson.subgroup == null ? 2.0 : 0.0))),
        child: Text(
          lesson.typeName,
          style: TextStyle(color: Colors.white),
        ),
        padding: EdgeInsets.fromLTRB(4.0, 2.0, 4.0, 2.0),
      ),
    );
    if (lesson.subgroup != null)
      typeWidgets.add(
        Container(
          decoration: BoxDecoration(
              color: Colors.deepPurple,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(2.0),
                  bottomRight: Radius.circular(2.0))),
          child: Text(
            lesson.subgroup,
            style: TextStyle(color: Colors.white),
          ),
          padding: EdgeInsets.fromLTRB(4.0, 2.0, 4.0, 2.0),
        ),
      );

    return Container(
        margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(color: Colors.deepPurple),
                      child: Text('${_timeBegin()}',
                          style:
                              TextStyle(fontSize: 16.0, color: Colors.white)),
                      margin: EdgeInsets.fromLTRB(8.0, 8.0, 0.0, 8.0),
                      padding: EdgeInsets.fromLTRB(4.0, 0.0, 4.0, 0.0),
                    ),
                    Container(
                      child: Text(' - ${_timeEnd()}',
                          style: TextStyle(
                              fontSize: 16.0, color: Colors.deepPurple)),
                      margin: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                    ),
                  ],
                ),
                Card(
                    shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    child: Row(children: typeWidgets))
              ],
            ),
            Container(
              padding: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 2.0),
              alignment: Alignment.centerLeft,
              child: Text(lesson.subject, style: _textStyle),
            ),
            Table(
              children: _lectorLocationList(),
            ),
          ],
        ));
  }
}
