import 'package:flutter/material.dart';
import 'package:flutter_schedule/entity/lesson.dart';
import 'package:flutter_schedule/entity/owner.dart';
import 'package:flutter_schedule/model/schedule_repository.dart';
import 'package:flutter_schedule/ui/lesson_widget_stls.dart';
import 'package:flutter_schedule/ui/owner_screen.dart';
import 'package:flutter_schedule/utils.dart' as utils;
import 'package:intl/intl.dart';

class ScheduleScreen extends StatefulWidget {
  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen>
    with SingleTickerProviderStateMixin {
  List<String> _tabs = List();
  List<List<Lesson>> _tabViews = List();

  TabController _tabController;
  var _dates = Map<String, List<Lesson>>();
  var _startTabIndex = DateTime.now().weekday - 1;
  var _currentTabIndex = 0;

  Owner _owner = null;

  @override
  void initState() {
    super.initState();
//    _tabs.add("Loading...");
    _fillingTabs();
    _tabController = TabController(length: 12, vsync: this);
    _tabController.index = _startTabIndex;
    _currentTabIndex = _startTabIndex;
    _tabController.addListener(() {
      /*setState(() {
        _currentTabIndex = _tabController.index;
      });*/
    });
    getOwner().then((owner) {
      if (owner == null) {
        _choiceMainOwner();
      } else {
        saveOwner(owner);
        _getSchedule(owner);
      }
    });
  }

  _setStartTab() {
    setState(() {
      _tabController.index = _startTabIndex;
    });
  }

  _fillingTabs() {
    var currentDate = DateTime.now();
    var formatter = DateFormat("yyyy-MM-dd");
    var mondayDate = currentDate.add(Duration(days: -currentDate.weekday + 1));
    for (var i = 0; i < 12; i++) {
      _tabs.add(formatter.format(mondayDate));
      mondayDate = mondayDate.add(Duration(days: 1));
      if (mondayDate.weekday == DateTime.sunday)
        mondayDate = mondayDate.add(Duration(days: 1));
    }
  }

  _choiceMainOwner() async {
    final resultOwner = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => OwnerScreen()));
    _getSchedule(resultOwner as Owner);
  }

  _getSchedule(Owner owner) {
    setState(() {
      _owner = owner;
    });
    getSchedule(owner).then(_initSchedule).catchError((err) => print(err));
  }

  _changeOwner() {}

  _searchOwner() {}

  _initSchedule(Map<String, List<Lesson>> dates) {
    setState(() {
      _dates = utils.fixSchedule(dates);
      print("kek");
    });
  }

  List<Lesson> _getLessons(String date) {
    try {
      return _dates[date] ?? List();
    } catch (e) {
      return List();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(_owner != null ? _owner.name : "Loading..."),
            bottom: TabBar(
                isScrollable: true,
                controller: _tabController,
                tabs: _tabs
                    .map((name) => Tab(
                          text: name,
                        ))
                    .toList())),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: AnimatedOpacity(
          opacity: _startTabIndex == _currentTabIndex ? 0.0 : 1.0,
          duration: Duration(milliseconds: 200),
          child: FloatingActionButton(
            child: const Icon(Icons.event_available),
            onPressed: _setStartTab,
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.menu),
                  onPressed: () {
                    showModalBottomSheet<void>(
                        context: context,
                        builder: (BuildContext context) {
                          return new Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new ListTile(
                                leading: new Icon(Icons.replay),
                                title:
                                    new Text('Изменить группу/преподавателя'),
                                onTap: () {},
                              ),
                              new ListTile(
                                leading: new Icon(Icons.share),
                                title: new Text('Поделиться'),
                                onTap: () {},
                              ),
                              new ListTile(
                                leading: new Icon(Icons.info),
                                title: new Text('О приложении'),
                                onTap: () {},
                              ),
                            ],
                          );
                        });
                  }),
              IconButton(
                icon: Icon(Icons.search),
                onPressed: _choiceMainOwner,
              ),
            ],
          ),
        ),
        body: TabBarView(
            controller: _tabController,
            children: _tabs
                .map(
                  (date) => _getLessons(date).length > 0
                      ? ListView.builder(
                          itemCount: _getLessons(date).length,
                          itemBuilder: (context, index) {
                            return LessonWidgetStls(
                              lesson: _getLessons(date)[index],
                              isGroup: _owner.isGroup,
                            );
                          },
                        )
                      : Center(
                          child: Text("Занятий нет"),
                        ),
                )
                .toList()));
  }
}
