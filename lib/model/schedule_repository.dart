import 'dart:convert';

import 'package:flutter_schedule/entity/lesson.dart';
import 'package:flutter_schedule/entity/owner.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<List<Owner>> getOwners() async {
  var client = new http.Client();
  var groupsResponse = await client.get(
      "https://schedule.vekclub.com/api/v1/handbook/groups-by-institution?institutionId=4");
  var lectorsResponse = await client.get(
      "https://schedule.vekclub.com/api/v1/handbook/lecturers-in-institution?institutionId=4");
  final groups = (json.decode(groupsResponse.body)["data"] as List)
      .map((json) => Owner.fromJson(json, true))
      .toList();
  final lectors = (json.decode(lectorsResponse.body)["data"] as List)
      .map((json) => Owner.fromJson(json, false))
      .toList();
  client.close();
  var list = List<Owner>();
  list.addAll(groups);
  list.addAll(lectors);
  return list;
}

Future<Map<String, List<Lesson>>> getSchedule(Owner owner) async {
  var client = new http.Client();
  var now = DateTime.now();
  now = now.add(Duration(days: -now.weekday + 1));
  var formatter = DateFormat("yyyy-MM-dd");
  var dateBegin = formatter.format(now);
  var dateEnd = formatter.format(now.add(Duration(days: 14)));
  var date = "&dateBegin=$dateBegin&dateEnd=$dateEnd";
  var link = owner.isGroup
      ? "group?groupId=${owner.id}"
      : "lector?lectorId=${owner.id}";
  var response = await client
      .get("https://schedule.vekclub.com/api/v1/schedule/$link$date");
  var datesMap = (json.decode(response.body)["data"] as Map<String, dynamic>);
  var dates = Map<String, List<Lesson>>();
  datesMap.forEach((key, val) {
    var lessons = List<Lesson>();
    var list = val as List;
    list.forEach((les) => lessons.add(Lesson.fromJson(les)));
    dates[key] = lessons;
  });
  return dates;
}

saveOwner(Owner owner) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var ownerStr = json.encode(owner.toJson());
  await prefs.setString("owner", ownerStr);
}

getOwner() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var ownerStr = prefs.getString("owner");
  if (ownerStr == null) return null;
  var owner = Owner.fromJson2(json.decode(ownerStr));
  return owner;
}
